const express = require('express');
const app = express();
const path = require('path');
const exec = require('child_process').exec;

app.use(express.static(path.join(__dirname, 'static')));

const executarPSO = (request) => {
  return new Promise((resolve, reject) => {
    exec(`java -jar ${'./static/Metaheuristics.jar'} shift_500.csv ${request.k} ${request.n} ${request.p} ${request.w} ${request.c1} ${request.c2} ${request.s} ${request.func}`, 
    (error, stdout, stderr) => {
      resolve(stdout);
      
      if (error) {
        reject(error);
      }
    });
  });
}

module.exports = executarPSO;