document.getElementById("calcular").addEventListener("click", function() {
  document.getElementById("resultado").innerHTML = "Calculando...";

  var k = document.getElementsByName("k")[0].value;
  var n = document.getElementsByName("n")[0].value;
  var p = document.getElementsByName("p")[0].value;
  var w = document.getElementsByName("w")[0].value;
  var c1 = document.getElementsByName("c1")[0].value;
  var c2 = document.getElementsByName("c2")[0].value;
  var s = document.getElementsByName("s")[0].value;
  var func = document.getElementsByName("func")[0].value;

  var params = { k, n, p, w, c1, c2, s, func }
  
  fetch("/calcular", {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(params),
  }).then(function(result) {
    return result.json();
  }).then(function(result){
    document.getElementById("resultado").innerText = result.data;
  });
});