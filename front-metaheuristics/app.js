const express = require('express');
const app = express();
const path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

const router = require('./router');
app.use('/', router);

module.exports = app;