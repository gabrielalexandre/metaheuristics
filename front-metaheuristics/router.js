const express = require('express');
const app = express();
const router = express.Router();
const bodyParser = require('body-parser');
const executarPSO = require('./pso.js');

app.use(express.static('public'));
app.use(bodyParser());

app.get('/', (req, res) => res.render('index.html'));

app.post('/calcular', (req, res) => {
  console.log("body " + JSON.stringify(req.body));
  const request = req.body;
  
  executarPSO(request).then((resultado) => {
    res.send({data: resultado});
  });
});

app.listen(3000, () => {
  console.log('Server started');
});

module.exports = router;