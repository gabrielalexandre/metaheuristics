/**
 * 
 */
package edu.uniandrade;

/**
 * @author Gabriel Alwxandre Lima Santos
 *
 */
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringReader;

/**
 * @brief Template for students to develop a metaheuristic search, based on a clustering algorithm.
 * 
 * Required WEKA tool.
 */
public class MetaMining {
    public static void main(String args[]) throws FileNotFoundException {
		if(args.length != 9){
			System.out.println("Faltou parametro: <shift-file> <k> <n> <p> <w> <c1> <c2> <s> <func>");
			return;
		}
		try {
			// Lê os parâmetros de inicialização
			String shiftFile = args[0];
			int k = Integer.parseInt(args[1]);
			int n = Integer.parseInt(args[2]);
			int p = Integer.parseInt(args[3]);
			double w = Double.parseDouble(args[4]);
			double c1 = Double.parseDouble(args[5]);
			double c2 = Double.parseDouble(args[6]);
			double s = Double.parseDouble(args[7]);
			String func = args[8];
			
			double nextPositions[][] = new double[p][n];
			double pbPositions[][];
			
			File newFile = new File("test.csv");
			PrintWriter pw = new PrintWriter(newFile);
	        StringBuilder sb = new StringBuilder();

			// Lê o arquivo de shift e multiplica por s.
			String csvFile = "src/" + shiftFile;
			String line = "";
			double[] shift = new double[n];
			
			// Leitura do arquivo de shift
			try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
				//Captura dos dados do arquivo com base na separação de colunas
				line = br.readLine();
				String[] shiftValue = line.split(",");
				
				// Incrementa obtenção dos dados de acordo com o valor de N
				for (int i = 0; i < n; i++) {
					// Recebe valores do arquivo e define o shift multiplicando cada dado pelo valor de S
					shift[i] = Double.parseDouble(shiftValue[i]) * s;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Inicializa a função de minimização e escolhe a função com base no valor de func
			MinFunction minimizacao = new MinFunction(shift, func);

			// Inicializa o PSO, passando a função de minimização
			PSO pso = new PSO(s, minimizacao, p, n, w, c1, c2);
			pso.setMaxEvals((int)1e6);

			int contadorSair = 0;
			// Procede com 1 execução do PSO.
			while(contadorSair < 2){
				double fitnessAnterior = pso.getGBestFitness(); 
				
				// Executa 2 * n iterações do PSO.
				pso.next(2 * n);
				
				// Verifica se o fitness melhorou.
				 if (fitnessAnterior == pso.getGBestFitness())
				 	contadorSair++;
				 else 
					contadorSair = 0;

				 if (k <= 0) {
					 continue;
				 }
				// Lê os melhores pessoais de cada indivíduo.
//				pbPositions = pso.getPBestPositions();

				// Monta o arquivo para o WEKA:
				// Construção do cabeçalho
				for (int i = 0; i < n; i++) {
					sb.append("x" + i);
					if (i != n - 1) sb.append(',');
				}
				
				// Construção das posições de cada indivíduo (P) nas dimensões informadas (N)
				double[][] pb = pso.getPBestPositions();
				for (int i = 0; i < p; i++) {
					sb.append("\n");
					for (int j = 0; j < n; j++) {
						sb.append(pb[i][j]);
						if (j != n - 1) sb.append(',');
					}
				}
				
				pw.write(sb.toString());
		        pw.close();
				
				// ... redireciona os streams
		        ByteArrayOutputStream bos = new ByteArrayOutputStream();
		        PrintStream printStream = new PrintStream(bos, true);
		        PrintStream oldStream = System.out;
		        System.setOut(printStream);

				// ... chama o algoritmo de clusterização do WEKA (SimpleKMeans).
				weka.clusterers.SimpleKMeans.main(new String[]{ "-init", "0", "-max-candidates", "100", "-periodic-pruning", "10000", "-min-density", "2.0", "-t1", "-1.25", "-t2", "-1.0", "-N", k+"", "-A", "weka.core.EuclideanDistance", "-I", "500", "-num-slots", "1", "-S", "10", "-t", newFile.getPath()});

				// ... restaura os streams.
				System.setOut(oldStream);
				
				String centroids = bos.toString();
				String formattedCentroids = centroids.substring(centroids.indexOf("x0"), centroids.indexOf("=== Clustering"));
				
				StringTokenizer st;
				BufferedReader br = new BufferedReader(new StringReader(formattedCentroids));
				double[][] centroidsArray = new double[k][n];
				
				// ... lê os centróides do texto retornado pelo WEKA, preenchendo as próximas posições em nextPostions.
				for (int i = 0; i < n; i++) {
					st = new StringTokenizer(br.readLine(), " ");
					st.nextToken();
					st.nextToken();
					for (int j = 0; j < k; j++) {
						centroidsArray[j][i] = Double.parseDouble(st.nextToken());
					}
				}
				
				for (int i = 0, j = 0; i < k; i++) {
					for (;((j+2) % (n/k)) != 0 && j < p; j++) {
						for (int l = 0; l < n; l++) {
							nextPositions[j][l] = centroidsArray[i][l] - s * (Math.random() * 0.3 - 0.15);
						}
					}
				}

				// Reposiciona os indivíduos do PSO nas novas posições.
				pso.setPositions(nextPositions);
			}
			System.out.println(pso.getGBestFitness());
			System.out.println(pso.getNEvals());
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
    }
}

