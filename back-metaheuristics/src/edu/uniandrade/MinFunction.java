package edu.uniandrade;

public class MinFunction extends FitnessFunction {

	private String func;

	MinFunction(double[] shift, String func) {
		super(shift);
		this.func = func;
	}

	@Override
	double evaluate(double[] pos) {
		double result = 0;
		double x;
		
		if (func.equalsIgnoreCase("SPHERE")) {
			for (int i = 0; i < pos.length; i++) {
				x = pos[i] - shift[i];
				result += Math.pow(x, 2) * i;
			}	
		} else if (func.equalsIgnoreCase("RASTRIGIN")) {
			result = 10 * pos.length;
			
			for (int i = 0; i < pos.length; i++){
				x = pos[i] - shift[i];
				result += Math.pow(x, 2) * i - Math.cos(Math.toRadians(2 * Math.PI * x * i));
			}
		}
		
		return result;
	}

}
