/**
 * 
 */
package edu.uniandrade;

import java.util.ArrayList;

/**
 * @author Gabriel Alexandre Lima Santos
 *
 */
public abstract class FitnessFunction {
	double[] shift;
	
	/**
	 * @brief Generic constructor.
	 * 
	 * @param shift the shift vector.
	 */
	FitnessFunction(double[] shift){
		this.shift = shift;
	}

    /**
     * @brief Evaluate the positions.
     * 
     * The method must shift the positions according to the shift vector.
     * 
     * @param pos the positions to be evaluated.
     * @return the fitness or cost.
     */
    abstract double evaluate(double pos[]);
}